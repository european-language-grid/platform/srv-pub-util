#!/bin/bash
abort()
{
    echo >&2 '
***************
*** ABORTED ***
***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}
set -eE
trap 'abort' ERR

error=0
if [ "$1" == "" ]; then error=1;fi
if [ $error -eq 1 ]; then
  echo
  echo "usage $0 <input file>"
  exit 1
fi
FILENAME=$1

#following options via command line or env
if [ "$2" != "" ]; then ELG_USER=$2;fi
if [ "$3" != "" ]; then ELG_TOKEN=$3;fi
if [ "$4" != "" ]; then SLACK_URL=$4;fi

CODE_EXISTING=0
CODE_COPIED=1
CODE_ERROR=2
CODE_CVE=3

declare -i COPIED=0
declare -i FAILURE=0
declare -i EXISTING=0
declare -i CVE=0

while read LINE; do 
    #extract source, target separated by ;
    TARGET=$(echo $LINE | sed 's/\(.*\)\(;\)//')
    SOURCE=$(echo $LINE | sed 's/\(;\)\(.*\)//')
    #call script, escape trap and save rc
    ./register-image.sh $SOURCE $TARGET $ELG_USER $ELG_TOKEN && :; RC=$?
    if [ $RC -eq $CODE_COPIED ] ; then
      msg="COPIED"
      echo -e "Syncing $SOURCE --> $TARGET [$msg]" >> /tmp/log2.txt
      COPIED+=1
    elif [ $RC -eq $CODE_EXISTING ]; then
      EXISTING+=1
    elif [ $RC -eq $CODE_CVE ]; then
      msg="CVE warning"
      echo -e "$TARGET [$msg]" >> /tmp/log2.txt
      CVE+=1
    else
      msg="FAILURE"
      echo -e "Syncing $SOURCE --> $TARGET [$msg]" >> /tmp/log2.txt
      FAILURE+=1 
    fi
#    echo "Syncing $SOURCE --> $TARGET [$msg]"
done < $FILENAME
if (( COPIED+FAILURE+CVE > 0)); then
  echo -e "Image Import: COPIED: $COPIED, EXISTING: $EXISTING, FAILURE: $FAILURE, CVE: $CVE" > /tmp/log1.txt
  MSG1=$(cat /tmp/log1.txt /tmp/log2.txt)
  SLACK_TEMPLATE=$( jq -n --arg msg "$MSG1" '{text: $msg}')
  echo $SLACK_TEMPLATE > /tmp/msg2.txt
  curl -s -X POST -H 'Content-type: application/json' $SLACK_URL --data-binary @/tmp/msg2.txt
fi
