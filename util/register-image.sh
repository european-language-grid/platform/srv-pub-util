#!/bin/bash
CODE_EXISTING=0
CODE_COPIED=1
CODE_ERROR=2
CODE_CVE=3

IMAGE_ORIGIN=$1
IMAGE_TARGET=$2
BOT_USER=$3
BOT_TOKEN=$4

#Extract registry.foo.com from registry.foo.com/image:tag
REPO_ORIGIN=$(echo $IMAGE_ORIGIN | sed 's/\(\/\)\(.*\)//')
REPO_TARGET=$(echo $IMAGE_TARGET | sed 's/\(\/\)\(.*\)//')

error=0
if [ "$1" == "" ] || [ "$2" == "" ] || [ "$3" == "" ] || [ "$4" == "" ]; then error=1;fi
if [ $error -eq 1 ]; then
  echo
  echo "usage $0 <repo_origin:tag> <repo_target:tag> <bot-user> <bot-pass>"
  exit 1
fi
echo "login", $REPO_TARGET

./regctl registry login -u $BOT_USER -p $BOT_TOKEN $REPO_TARGET
image_exists=$( ./regctl image inspect $IMAGE_TARGET 2>&1 )
case $image_exists in
  *"created"*) 
    exit $CODE_EXISTING
    ;;
  *"[http 412]"*)
    exit $CODE_CVE
    ;;
  *"NOT_FOUND"*)
    if ./regctl image copy -v debug $IMAGE_ORIGIN $IMAGE_TARGET; then
      exit $CODE_COPIED
    else
      exit $CODE_ERROR
    fi
    ;;
  *)
    echo "Unknown Error - 'regctl image inspect' output follows"
    echo "$image_exists"
    exit $CODE_ERROR  
esac
