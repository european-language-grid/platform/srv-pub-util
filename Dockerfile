from ubuntu:20.04
RUN apt-get update
#RUN apt-get -y install openssh-client
#RUN apt-get -y install ca-certificates
RUN apt-get -y install curl
#RUN mkdir /root/.ssh && ssh-keyscan -H gitlab.com >> /root/.ssh/known_hosts
RUN apt-get install -y --force-yes jq

WORKDIR /app
RUN curl -s -L https://github.com/regclient/regclient/releases/latest/download/regctl-linux-amd64 >regctl
RUN chmod 755 regctl

COPY /util/register-image.sh /app
COPY /util/register-all.sh /app

RUN chmod +x /app/register-image.sh


ENTRYPOINT ["/bin/bash"]
CMD ["/app/register-all.sh", "arg1"]
